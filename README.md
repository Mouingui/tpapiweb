# TPAPIWeb
#MOUINGUI Destinée 

# Mon Projet de Site Web TPAPIWeb

Ce projet est un site web qui exploite l'API de data.gouv.fr "Prix des carburants" et affiche les résultats sur une carte OpenStreetMap en utilisant PHP, JavaScript (avec Leaflet), et des librairies CSS.

#ce site interroge l'API  l'API de data.gouv.fr pour obtenir des données et les affiche sur une carte OpenStreetMap. Les utilisateurs peuvent rechercher des informations spécifiques en fonction de leurs besoins, lien de l'API : 
#https://data.economie.gouv.fr/api/explore/v2.1/catalog/datasets/prix-des-carburants-en-france-flux-instantane-v2/records?limit=20

## Installation

1. Il faut Cloner ce dépôt sur notre  machine locale :

```bash
git clone https://gitlab.com/Mouingui/tpapiweb.git




## Fonctionnalités

- Recherche de données à partir de l'API Data.gouv.fr
- Affichage des résultats sur une carte OpenStreetMap

## Utilisation

    Lancez l'application en ouvrant le fichier index.html dans un navigateur web.

    Utilisez le formulaire de recherche pour interagir avec l'API de data.gouv.fr et afficher les résultats sur la carte OpenStreetMap.


    ##Pour Naviguer vers le répertoire du projet :

bash

cd C:\xampp\htdocs\dev

    Installation les dépendances (le cas échéant) :

bash

npm install

    Pour Obtenir une clé d'API de data.gouv.fr il faut suivre les instructions sur leur site.

    il Créer un fichier de configuration .env à la racine du projet et ajouter la clé d'API :

env

DATA_GOUV_API_KEY=votre-clé-api 
Pour ma part je n'ai pas utilisé la clé d'API


